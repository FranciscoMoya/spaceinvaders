# Space Invaders

GNU/Linux minigame developed in C++ by means of [Ogre 1.9](http://www.ogre3d.org/), [CEGUI](http://cegui.org.uk/) and [CEED](http://cegui.org.uk/wiki/CEED).

## Authors
* Pedro Manuel Gómez-Portillo López
* José Juan Ramírez de Arellano Díaz Miguel

## Screenshots

### Spash screen
![Splash screen](media/screenshots/splash_screen.png)

### Main menu
![Main menu](media/screenshots/main_menu.png)

### Credits
![Credits](media/screenshots/credits.png)

### Enemy waves
![Enemies](media/screenshots/enemies.png)

### Final boss
![Final boss](media/screenshots/final_boss.png)

## License

This program and its files are distributed under the [MIT License](LICENSE.txt).
